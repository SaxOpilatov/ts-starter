# Starter pack
## Node.js + TypeScript + Webpack

```
$ npm install
$ npm start
```

## Work dir
```
./src
```

## Compiled file
```
./dist/index.js
```